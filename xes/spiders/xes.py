# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import urljoin
import datetime
import hashlib
from scrapy.http import Request
from xes.items import xesItem,xes_city_Item,xes_class_addr_Item
from lxml import etree
import re
import json
import requests
def get_md5(url):
    m = hashlib.md5()
    m.update(url)
    return m.hexdigest()

def retry_http(func,data,rery_num=3):
    for i in range(rery_num):
        try:
            return func(*data)
        except:
            if i == rery_num-1:
                return None

def get_all_region():
    url = 'http://sbj.speiyou.com/search'
    res = requests.get(url)
    htmlEmt = etree.HTML(res.text)
    rlist = htmlEmt.xpath('//*[@id="tb-site-list"]/dl/dd/a/@href')
    reval = []
    for i in rlist:
        index = i.find('://')
        reval.append(i[index+3:])
    reval = [url[7:] for url in rlist if url[7:] not in 'http://app.speiyou.com']
    # reval.append('fz.speiyou.com')
    reval.append('nc.speiyou.com')
    return reval

def get_urls(alloweds):
    for allowed in alloweds:
        init_url = 'http://%s/search/index' % allowed
        res = retry_http(requests.get,(init_url + '/subject:/grade:/level:bx/lesson:/term:/gtype:time',))
        htmlEmt = etree.HTML(res.text)
        rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[1]/dd/ul/li/a/@href')  # 年级
        for l in rlist:
            res = re.findall('grade:([\s\S]*?)/', l)
            if res != ['']:
                i = res[0]
                url = init_url + '/subject:/grade:%s/level:bx/lesson:/term:/gtype:time' % i
                data = [i, [], [], [], []]
                res = retry_http(requests.get,(url,))
                htmlEmt = etree.HTML(res.text)

                # rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[3]/dd/ul/li/a/@href')  # 班次
                # for l in rlist:
                #     res = re.findall('level:([\s\S]*?)/', l)
                #     if res != [''] and res[0] != 'bx':
                #         data[2].append(res[0])

                # rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[4]/dd/ul/li/a/@href')  # 班级类型
                # for l in rlist:
                #     res = re.findall('term:([\s\S]*?)/', l)
                #     if res != ['']:
                #         data[3].append(res[0])

                rlist = htmlEmt.xpath('//*[@id="search-term"]/dl[5]/dd/ul/li/a/@href')  # 授课类型
                for l in rlist:
                    res = re.findall('lesson:([\s\S]*?)/', l)
                    if res != ['']:
                        data[4].append(res[0])

                # yield from (init_url + '/grade:%s/subject:/level:%s/lesson:%s/term:%s/gtype:time\n' % (i, b, d, c,)
                #          for b in data[2] for c in data[3] for d in data[4])
                yield from (init_url + '/grade:%s/subject:/lesson:%s/gtype:time\n' % (i, d,)
                            for d in data[4])

class XesSpider(scrapy.Spider):
    headers  = {'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
                'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                'Accept-Encoding': 'deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9'}
    url_set = set()
    allowed_domains = get_all_region()
    name = 'xes'
    start_urls = ['http://sbj.speiyou.com/search',]
    other_allowed_domains = ['nc.speiyou.com',] # 'fz.speiyou.com',

    def parse(self, response):
        citys = response.xpath('//*[@id="tb-site-list"]/dl/dd/a/text()').extract()
        for city in citys:
            item = xes_city_Item()
            item['city'] = city
            item['date'] = datetime.datetime.now()
            yield item

        for url in self.allowed_domains:
            yield Request('http://%s/search/index/grade:3/subject:/level:bx/lesson:/gtype:place' % url, headers=self.headers, callback=self.handle_class_addr_data,dont_filter=True)

        for url in get_urls(self.allowed_domains):
            yield Request(url, headers=self.headers,callback=self.handle_data)

    def handle_class_addr_data(self,response):
        res = re.findall('AREADATA = (.*?);',response.text)
        city = response.xpath('//*[@id="tb-site"]/span/text()').extract_first()
        try:
            js_obj = json.loads(res[0])

            for a in js_obj:
                for b in js_obj[a]['de']:
                    item = xes_class_addr_Item()
                    item['city'] = city
                    item['dept_name'] = b['dept_name']
                    item['dept_address'] = b['dept_address']
                    item['dept_latitude'] = b['dept_latitude']
                    item['dept_longitude'] = b['dept_longitude']
                    item['dept_phone'] = b['dept_phone']
                    item['date'] = datetime.datetime.now()
                    yield item
        except IndexError:
            pass

    def handle_data(self,response):
        data = {}
        data['city'] = response.xpath('//*[@id="tb-site"]/span/text()').extract_first()
        data['grade'] = response.xpath('//*[@id="search-term"]/dl[1]/dd/ul/li[@class="select"]/a/text()').extract_first()
        data['level'] = response.xpath('//*[@id="search-term"]/dl[3]/dd/ul/li[@class="select"]/a/text()').extract_first()
        data['term'] = response.xpath('//*[@id="search-term"]/dl[4]/dd/ul/li[@class="select"]/a/text()').extract_first()
        data['lesson'] = response.xpath('//*[@id="search-term"]/dl[5]/dd/ul/li[@class="select"]/a/text()').extract_first()
        if data['lesson'] != '不限':
            items = response.xpath('//section[@class="s-main-box"]/div[@class="s-r-list"]')
            for item in items:
                data['subject'] = item.xpath('./div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/p[1]/span[1]/text()').extract_first()
                data['subject'] = re.findall('学科：(.*?)$',data['subject'])[0].strip()
                data['teacher'] = item.xpath('./div[@class="s-r-list-photo"]/p/a/text()').extract_first()
                data['price'] = item.xpath(
                    './div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/div[@class="price"]/span/text()').extract_first()
                data['class_name'] = item.xpath(
                    './div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/h3/a/text()').extract_first()
                url = item.xpath('./div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/h3/a/@href').extract_first()
                date = item.xpath(
                    './div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/p[2]/span[1]/text()').extract_first()
                res = re.findall(r'开课日期：([\s\S]*?)至([\s\S]*?)$', date)
                try:
                    data['start_date'] = res[0][0]
                except:
                    data['start_date'] = ''
                try:
                    data['end_date'] = res[0][1]
                except:
                    data['end_date'] = ''

                time = item.xpath(
                    './div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/p[2]/span[2]/text()').extract_first()
                try:
                    data['time'] = re.findall(r'上课时间：([\s\S]*?)$', time)[0]
                except:
                    data['time'] = None
                try:
                    res = re.findall(r'.*?(\d[\s\S]*?):([\s\S]*?)-(\d[\s\S]*?):([\s\S]*?)$', data['time'])
                    data['class_time_long'] = ((int(res[0][2]) * 60 + int(res[0][3])) - (int(res[0][0]) * 60 + int(res[0][1]))) / 60
                except:
                    data['class_time_long'] = 2.0
                class_addr = item.xpath('./div[@class="s-r-list-detail"]/div[@class="s-r-list-info"]/p[3]/text()').extract_first()
                try:
                    data['class_addr'] = re.findall(r'上课地点：([\s\S]*?)$', class_addr)[0]
                except:
                    data['class_addr'] = ''
                data['state'] = item.xpath('./div[@class="s-r-list-detail"]/div[3]/p[1]/span/text()').extract_first()
                url = urljoin(response.url, url)
                m = int(get_md5(url=url.encode()), base=16)
                if m not in self.url_set:
                    self.url_set.add(m)
                    yield Request(url=urljoin(response.url, url), meta=data, headers=self.headers,callback=self.detail)

        item = response.xpath('//div[contains(@class,"pagination")]/a/@href')
        urls = [urljoin(response.url, url) for url in item.extract()]
        for url in urls:
            # http://sbj.spindex/gtype:time/grade:-8/subject:/level:bx/lesson:4/term:/period:/teaid:/m:/d:/time:/bg:n/nu:/service:/curpage:2
            if re.findall('index/gtype:time/grade:.+/subject:/level:bx/lesson:.+/term:/period:.*/teaid:.*/m:/d:.*/time:.*/bg:.*/nu:.*/service:.*/curpage:\d+$',url):
                m = int(get_md5(url=url.encode()), base=16)
                if m not in self.url_set:
                    self.url_set.add(m)
                    yield Request(url=url,headers=self.headers, callback=self.handle_data)

    def detail(self, response):
        data = response.meta
        data['class_num'] = len(response.xpath('//*[@id="con_one_1"]/ul/li').extract())
        data['all_class_time_long'] = data['class_num']*data['class_time_long']
        item = xesItem()
        item['city'] = data['city']
        item['grade'] = data['grade']
        item['subject'] = data['subject']
        item['level'] = data['level']
        item['term'] = data['term']
        item['lesson'] = data['lesson']
        item['teacher'] = data['teacher']
        item['price'] = data['price']
        item['class_name'] = data['class_name']
        item['start_date'] = data['start_date']
        item['end_date'] = data['end_date']
        item['time'] = data['time']
        item['class_time_long'] = data['class_time_long']
        item['class_addr'] = data['class_addr']
        item['state'] = data['state']
        item['class_num'] = data['class_num']
        item['all_class_time_long'] = data['all_class_time_long']
        yield item

