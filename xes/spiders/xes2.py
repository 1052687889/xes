# -*- coding: utf-8 -*-
import scrapy
from xes.items import xes2_basis_ItemLoader
import datetime
from scrapy.http import Request
from xes.items import xes2_basis_Item,xes2_detail_Item,xes2_detail_ItemLoader
from scrapy.loader import ItemLoader

class Xes2Spider(scrapy.Spider):
    headers = {'Host': 'www.xueersi.com',
               'Connection': 'keep-alive',
               'Cache-Control': 'max-age=0',
               'Upgrade-Insecure-Requests': '1',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
               'Accept-Encoding': 'deflate, br',
               'Accept-Language': 'zh-CN,zh;q=0.9'}
    allowed_domains = ['www.xueersi.com',]
    name = 'xes2'
    start_urls = ['https://www.xueersi.com/chu1-0-4',]

    def parse(self, response):
        urls = response.xpath('/html/body/div[2]/div[1]/div/dl[2]/dd/a/@href').extract()
        for url in urls:
            yield Request(url=url,callback=self.handle_subject,headers=self.headers)


    def handle_subject(self,response):
        urls = response.xpath('/html/body/div[2]/div[1]/div/dl[3]/dd/a/@href').extract()[1:]
        for url in urls:
            yield Request(url=url, callback=self.handle_difficult, headers=self.headers)

    def handle_difficult(self,response):
        urls = response.xpath('/html/body/div[2]/div[1]/div/dl[4]/dd/a/@href').extract()[1:]
        for url in urls:
            yield Request(url=url, callback=self.handle_term, headers=self.headers)


    def handle_term(self, response):
        urls = response.xpath('/html/body/div[2]/div[1]/div/dl[5]/dd/a/@href').extract()[1:]
        for url in urls:
            yield Request(url=url, callback=self.handle_time, headers=self.headers)


    def handle_time(self,response):
        urls = response.xpath('/html/body/div[2]/div[1]/div/dl[6]/dd/a/@href').extract()[1:]
        for url in urls:
            yield Request(url=url, callback=self.handle_time_slot, headers=self.headers)


    def handle_time_slot(self,response):
        urls = response.xpath('/html/body/div[2]/div[1]/div/dl[7]/dd/a/@href').extract()[1:]
        for url in urls:
            yield Request(url=url, callback=self.handle_detail, headers=self.headers)

    def handle_detail(self,response):
        item_loader = xes2_basis_ItemLoader(item=xes2_basis_Item(), response=response)
        item_loader.add_xpath("class_type", '/html/body/div[2]/div[1]/div/dl[1]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("grade", '/html/body/div[2]/div[1]/div/dl[2]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("subject", '/html/body/div[2]/div[1]/div/dl[3]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("difficult", '/html/body/div[2]/div[1]/div/dl[4]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("term", '/html/body/div[2]/div[1]/div/dl[5]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("time", '/html/body/div[2]/div[1]/div/dl[6]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("time_slot", '/html/body/div[2]/div[1]/div/dl[7]/dd/a[@class="active"]/text()')
        item_loader.add_xpath("class_num", '//li[@class="Onlylive-select"]/strong[1]/text()')
        item_loader.add_xpath("student_num", '//li[@class="Onlylive-select"]/strong[2]/text()')
        item_loader.add_value("date", [datetime.datetime.now().date()])
        yield item_loader.load_item()

        urls = response.xpath('//div[@class="xue-block-grid"]/ul/li/a/@href').extract()
        for url in urls:
            data = {'class_type':item_loader.get_collected_values('class_type'),
                    'grade': item_loader.get_collected_values('grade'),
                    'subject': item_loader.get_collected_values('subject'),
                    'difficult': item_loader.get_collected_values('difficult'),
                    'term': item_loader.get_collected_values('term')}
            yield Request(url=url, callback=self.detail, meta=data,headers=self.headers)

    def detail(self,response):
        data = response.meta
        item_loader = xes2_detail_ItemLoader(item=xes2_detail_Item(),response=response)
        item_loader.add_xpath('title','/html/body/div[2]/div[2]/div[1]/div[1]/div/p[2]/text()')
        item_loader.add_xpath('course_num', '//*[@id="scroll-box-wrap"]/ul/li[2]/a/text()')
        item_loader.add_xpath('remain', '/html/body/div[2]/div[2]/div[1]/div[1]/ul/li[2]/p/span/text()')
        item_loader.add_xpath('instructor', '//*[@id="teacher-box"]/a/div[2]/p[1]/text()')
        item_loader.add_xpath('tutor', '/html/body/div[2]/div[2]/div[1]/div[1]/ul/li[2]/div/div[2]/p[1]/text()')
        item_loader.add_xpath('useful_date_start', '/html/body/div[2]/div[2]/div[1]/div[1]/div/div/p[2]/text()')
        item_loader.add_xpath('useful_date_end', '/html/body/div[2]/div[2]/div[1]/div[1]/div/div/p[2]/text()')
        item_loader.add_xpath('course_time', '/html/body/div[2]/div[2]/div[1]/div[1]/div/p[1]/strong/text()')
        item_loader.add_xpath('price', '//p[@class="course-continue-price"]/strong/text()')
        #                               /html/body/div[2]/div[2]/div/div[3]/div[1]/p/strong
        item_loader.add_value('class_type', data.get('class_type'))
        item_loader.add_value('grade', data.get('grade'))
        item_loader.add_value('subject', data.get('subject'))
        item_loader.add_value('difficult', data.get('difficult'))
        item_loader.add_value('term', data.get('term'))
        item_loader.add_value('date', [datetime.datetime.now().date()])
        yield item_loader.load_item()































