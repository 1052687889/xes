# -*- coding: utf-8 -*-
import scrapy
import urllib
import json
import datetime
from scrapy.http import Request
from xes.items import xes_app_Item
def get_type_from_title(title):
    if title == '':
        return ''
    if '在线' in title:
        return '在线'
    if '双师' in title:
        return '双师'
    return '面授'

class XesAppSpider(scrapy.Spider):
    name = 'xes_app'
    allowed_domains = ['xesapi.speiyou.cn']
    start_urls = ['https://xesapi.speiyou.cn/v1/city/list',]
    data = {
        'token': 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmZmZmZmZmZi1hYTQ0LTAwYjUtZmZmZi1mZmZmYTg3ZjI1ZTAifQ.MMLAAwXTzHPzSyWT_rBWAKbSWSb2rDC-iZUGaQyt85yLRU_GQt67m_EHXjieH2CkM32ZCazuxDDeO5Yh5_MG-g',
        'v': '6.9.0',
        'client_type': '2',
        'distinct_id': 'ff8080816870cd6a0168a1c12f764a0c',
        'lat': '',
        'lng': ''
    }
    def start_requests(self):
        for url in self.start_urls:
            yield Request(url+'?'+urllib.parse.urlencode(self.data), method='POST',callback=self.parse,dont_filter=True)

    def parse(self, response):
        js_obj = json.loads(response.text)
        for city in js_obj['data']:
            headers = {
                'X-Tingyun-Id': 'zAj7Vu-0QAI;c=2;r=2049416939;',
                'Authorization': 'TouristBearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmZmZmZmZmZi1hYTQ0LTAwYjUtZmZmZi1mZmZmYTg3ZjI1ZTAifQ.MMLAAwXTzHPzSyWT_rBWAKbSWSb2rDC-iZUGaQyt85yLRU_GQt67m_EHXjieH2CkM32ZCazuxDDeO5Yh5_MG-g',
                'Accept-Language': 'zh-CN',
                'gradeId': '1',
                'area': city['city_id'],
                'v': '6.9.0',
                'client_type': '2',
                'devid': 'ffffffff-aa44-00b5-ffff-ffffa87f25e0',
                'sv': '5.1.1',
                'Host': 'xesapi.speiyou.cn',
                'Connection': 'Keep-Alive',
                'Accept-Encoding': 'gzip',
                'User-Agent': 'okhttp/3.6.0',
            }
            meta = {'city_id':city['city_id'],
                    'city_name':city['city_name']}
            yield Request('https://xesapi.speiyou.cn/v1/py/grade/list'+'?'+urllib.parse.urlencode(self.data),headers=headers,meta=meta,callback=self.handle_grade_list,dont_filter=True)

    def handle_grade_list(self,response):
        meta = response.meta
        js_obj = json.loads(response.text)
        for grd in js_obj['data']:
            for i in grd['data']:
                meta['page'] = 1
                meta['grd_id'] = i['grd_id']
                meta['grd_name'] = i['grd_name']
                headers = {
                    'X-Tingyun-Id': 'zAj7Vu-0QAI;c=2;r=2049416939;',
                    'Authorization': 'TouristBearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmZmZmZmZmZi1hYTQ0LTAwYjUtZmZmZi1mZmZmYTg3ZjI1ZTAifQ.MMLAAwXTzHPzSyWT_rBWAKbSWSb2rDC-iZUGaQyt85yLRU_GQt67m_EHXjieH2CkM32ZCazuxDDeO5Yh5_MG-g',
                    'Accept-Language': 'zh-CN',
                    'gradeId': '1',
                    'area': meta['city_id'],
                    'v': '6.9.0',
                    'client_type': '2',
                    'devid': 'ffffffff-aa44-00b5-ffff-ffffa87f25e0',
                    'sv': '5.1.1',
                    'Host': 'xesapi.speiyou.cn',
                    'Connection': 'Keep-Alive',
                    'Accept-Encoding': 'gzip',
                    'User-Agent': 'okhttp/3.6.0',
                }
                params = {
                    "gradeId": i['grd_id'],
                    "device": "android",
                    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmZmZmZmZmZi1hYTQ0LTAwYjUtZmZmZi1mZmZmYTg3ZjI1ZTAifQ.MMLAAwXTzHPzSyWT_rBWAKbSWSb2rDC-iZUGaQyt85yLRU_GQt67m_EHXjieH2CkM32ZCazuxDDeO5Yh5_MG-g",
                    "year": "",
                    "timeType": "0",
                    "subjectIds": "",
                    "claCourseType": "0",
                    "lev_degree": "",
                    "cla_term_id": "",
                    "page": str(meta['page']),
                    "isHiddenFull": "0"
                }
                yield scrapy.FormRequest('https://xesapi.speiyou.cn/v1/py/course/list' + '?' + urllib.parse.urlencode(self.data),
                                         method = 'POST', headers = headers, formdata=params,callback=self.handle_course_list,dont_filter=True,meta=meta)

    def handle_course_list(self,response):
        meta = response.meta
        js_obj = json.loads(response.text)
        reval_count = js_obj['data']['queryCount']
        for _class in js_obj['data']['queryData']:
            item = xes_app_Item()
            item['city'] = meta['city_name']
            item['cla_name'] = _class.get('cla_name', '')
            item['cla_teacher_names'] = _class.get('cla_teacher_names', '')
            item['cla_end_date'] = _class.get('cla_end_date', '')
            item['d_name'] = _class.get('d_name', '')
            item['cla_tutor_real_name'] = _class.get('cla_tutor_real_name', '')
            item['cla_start_date'] = _class.get('cla_start_date', '')
            item['cla_classtime_names'] = _class.get('cla_classtime_names', '')
            item['cla_classroom_name'] = _class.get('cla_classroom_name', '')
            item['cla_venue_name'] = _class.get('cla_venue_name', '')
            item['cla_area_name'] = _class.get('cla_area_name', '')
            item['cla_price'] = _class.get('cla_price', '')
            item['dept_name'] = _class.get('dept_name', '')
            item['cla_surplus_persons'] = _class.get('cla_surplus_persons', '')
            item['_type'] = get_type_from_title(_class['cla_name'])
            item['cla_quota_num'] = _class.get('cla_quota_num', '')
            item['date'] = str(datetime.datetime.now().date())
            yield item
        if reval_count > meta['page']*10:
            meta['page'] += 1
            headers = {
                'X-Tingyun-Id': 'zAj7Vu-0QAI;c=2;r=2049416939;',
                'Authorization': 'TouristBearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmZmZmZmZmZi1hYTQ0LTAwYjUtZmZmZi1mZmZmYTg3ZjI1ZTAifQ.MMLAAwXTzHPzSyWT_rBWAKbSWSb2rDC-iZUGaQyt85yLRU_GQt67m_EHXjieH2CkM32ZCazuxDDeO5Yh5_MG-g',
                'Accept-Language': 'zh-CN',
                'gradeId': '1',
                'area': meta['city_id'],
                'v': '6.9.0',
                'client_type': '2',
                'devid': 'ffffffff-aa44-00b5-ffff-ffffa87f25e0',
                'sv': '5.1.1',
                'Host': 'xesapi.speiyou.cn',
                'Connection': 'Keep-Alive',
                'Accept-Encoding': 'gzip',
                'User-Agent': 'okhttp/3.6.0',
            }
            params = {
                "gradeId": meta['grd_id'],
                "device": "android",
                "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmZmZmZmZmZi1hYTQ0LTAwYjUtZmZmZi1mZmZmYTg3ZjI1ZTAifQ.MMLAAwXTzHPzSyWT_rBWAKbSWSb2rDC-iZUGaQyt85yLRU_GQt67m_EHXjieH2CkM32ZCazuxDDeO5Yh5_MG-g",
                "year": "",
                "timeType": "0",
                "subjectIds": "",
                "claCourseType": "0",
                "lev_degree": "",
                "cla_term_id": "",
                "page": str(meta['page']),
                "isHiddenFull": "0"
            }
            yield scrapy.FormRequest(
                'https://xesapi.speiyou.cn/v1/py/course/list' + '?' + urllib.parse.urlencode(self.data),
                method='POST', headers=headers, formdata=params, callback=self.handle_course_list, dont_filter=True,meta=meta)

