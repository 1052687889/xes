# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
import datetime
import hashlib
import re
from scrapy.loader import ItemLoader

def get_md5(url):
    m = hashlib.md5()
    m.update(url)
    return m.hexdigest()
class xesItem(scrapy.Item):
    city = scrapy.Field()
    grade = scrapy.Field()
    subject = scrapy.Field()
    level = scrapy.Field()
    term = scrapy.Field()
    lesson = scrapy.Field()
    teacher = scrapy.Field()
    price = scrapy.Field()
    class_name = scrapy.Field()
    start_date = scrapy.Field()
    end_date = scrapy.Field()
    time = scrapy.Field()
    class_time_long = scrapy.Field()
    class_addr = scrapy.Field()
    state = scrapy.Field()
    class_num = scrapy.Field()
    all_class_time_long = scrapy.Field()
    date = scrapy.Field()
    hash = scrapy.Field()

    def get_insert_sql(self):
        sql = """
           insert into xes_data(city, grade, subject, level, term, lesson, teacher, price, class_name, start_date, end_date,time,class_time_long,class_addr,state,class_num,all_class_time_long,date,hash)
           values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s,%s);
        """
        params = (
            self["city"], self["grade"], self["subject"],
            self["level"], self["term"], self["lesson"],
            self["teacher"],self["price"], self["class_name"],
            self["start_date"],self["end_date"],self["time"],
            self["class_time_long"],self["class_addr"],self["state"],
            self["class_num"],self["all_class_time_long"],datetime.datetime.now().date(),
            get_md5((self["city"]+self["grade"]+self["subject"]+
            self["level"]+self["term"]+self["lesson"]+self["teacher"]+self['time']+self['class_addr']).encode())
        )
        return sql, params

class xes_city_Item(scrapy.Item):
    city = scrapy.Field()
    date = scrapy.Field()

    def get_insert_sql(self):
        sql = '''insert into xes_city (city,date) values(%s,%s);
        '''
        params = (
            self['city'],self['date']
        )
        return sql,params

class xes_class_addr_Item(scrapy.Item):
    city = scrapy.Field()
    dept_name = scrapy.Field()
    dept_address = scrapy.Field()
    dept_latitude = scrapy.Field()
    dept_longitude = scrapy.Field()
    dept_phone = scrapy.Field()
    date = scrapy.Field()
    def get_insert_sql(self):
        sql = '''insert into xes_class_addr (city,dept_name,dept_address,dept_latitude,dept_longitude,dept_phone,date) values(%s,%s,%s,%s,%s,%s,%s);
        '''
        params = (
            self['city'],self['dept_name'],self['dept_address'],self['dept_latitude'],self['dept_longitude'],self['dept_phone'],self['date']
        )
        return sql,params


class DataStrip(object):

    def __call__(self, values):
        if values:
            for value in values:
                if value is not None and value != '':
                    yield value.strip()
        else:
            yield ''
class TakeFirst(object):

    def __call__(self, values):
        if values:
            for value in values:
                if value is not None and value != '':
                    return value
        else:
            return ''

def date_input_processor(data):
    return data

class xes2_basis_ItemLoader(ItemLoader):
    default_output_processor = TakeFirst()
    default_input_processor = DataStrip()

class xes2_basis_Item(scrapy.Item):
    class_type = scrapy.Field()
    grade = scrapy.Field()
    subject = scrapy.Field()
    difficult = scrapy.Field()
    term = scrapy.Field()
    time = scrapy.Field()
    time_slot = scrapy.Field()
    class_num = scrapy.Field()
    student_num = scrapy.Field()
    date = scrapy.Field(input_processor=date_input_processor)

    def get_insert_sql(self):
        sql = """
           insert into xes2_basis(class_type, grade, subject, difficult, term, time, time_slot, class_num, student_num,date)
           values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        """
        params = (
            self["class_type"], self["grade"], self["subject"],
            self["difficult"], self["term"], self["time"],
            self["time_slot"],self["class_num"], self["student_num"],
            self["date"],
        )
        return sql, params

def title_input_processor(data):
    reval=data
    return reval

def remain_input_processor(datas):
    if datas:
        for data in datas:
            res = re.findall('剩余([\s\S]*?)个名额', data.strip())
            if res:
                yield int(res[0])
            else:
                yield 0
    else:
        yield 0

def tutor_input_processor(datas):
    if datas:
        for value in datas:
            if value is not None and value != '':
                yield value.strip()
            else:
                yield ''
    else:
        return ['']

def course_num_input_processor(datas):
    for data in datas:
        res = re.findall('课程大纲\(([\s\S]*?)\)', data.strip())
        if res:
            yield int(res[0])
        else:
            yield 0

def useful_date_start_input_processor(datas):
    if datas:
        for data in datas:
            res = re.findall('期：([\s\S]*?)至',data.strip())
            if res:
                yield datetime.datetime.strptime(res[0], "%Y-%m-%d").date()
            else:
                yield datetime.datetime.now().date()
    else:
        yield datetime.datetime.now().date()

def useful_date_end_input_processor(datas):
    if datas:
        for data in datas:
            res = re.findall('至([\s\S]*?)$', data.strip())
            if res:
                yield datetime.datetime.strptime(res[0], "%Y-%m-%d").date()
            else:
                yield datetime.datetime.now().date()
    else:
        yield datetime.datetime.now().date()

def price_input_processor(datas):
    if datas:
        for data in datas:
            yield int(data)
    else:
        yield 0

class xes2_detail_ItemLoader(ItemLoader):
    default_input_processor = DataStrip()
    default_output_processor = TakeFirst()


class xes2_detail_Item(scrapy.Item):
    title = scrapy.Field()
    course_num = scrapy.Field(input_processor=course_num_input_processor)
    remain = scrapy.Field(input_processor=remain_input_processor)
    instructor = scrapy.Field()
    tutor = scrapy.Field(input_processor=tutor_input_processor)
    useful_date_start = scrapy.Field(input_processor=useful_date_start_input_processor)
    useful_date_end = scrapy.Field(input_processor=useful_date_end_input_processor)
    course_time = scrapy.Field()
    price = scrapy.Field(input_processor=price_input_processor)
    class_type = scrapy.Field()
    grade = scrapy.Field()
    subject = scrapy.Field()
    difficult = scrapy.Field()
    term = scrapy.Field()
    date = scrapy.Field(input_processor=date_input_processor)

    def get_insert_sql(self):
        sql = """
           insert into xes2_detail(title, course_num, remain, instructor, tutor, useful_date_start, 
                                   useful_date_end, course_time, price,class_type,grade,subject,
                                   difficult,term,date)
           values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s);
        """
        params = (
            self["title"], self["course_num"], self["remain"],
            self["instructor"], self["tutor"], str(self["useful_date_start"]),
            str(self["useful_date_end"]),self["course_time"], self["price"],
            self["class_type"],self["grade"],self["subject"],self["difficult"],self["term"],self["date"],
        )
        return sql, params


class xes_app_Item(scrapy.Item):
    city = scrapy.Field()
    cla_name = scrapy.Field()
    cla_teacher_names = scrapy.Field()
    cla_start_date = scrapy.Field()
    cla_end_date = scrapy.Field()
    d_name = scrapy.Field()
    cla_tutor_real_name = scrapy.Field()
    cla_classtime_names = scrapy.Field()
    cla_classroom_name = scrapy.Field()
    cla_venue_name = scrapy.Field()
    cla_area_name = scrapy.Field()
    cla_price = scrapy.Field()
    dept_name = scrapy.Field()
    cla_surplus_persons = scrapy.Field()
    cla_quota_num = scrapy.Field()
    _type = scrapy.Field()
    date=scrapy.Field()

    def get_insert_sql(self):
        sql = """
           insert into xes_app(city, cla_name, cla_teacher_names, cla_start_date, cla_end_date, d_name, 
                                   cla_tutor_real_name, cla_classtime_names, cla_classroom_name,cla_venue_name,cla_area_name,cla_price,dept_name,
                                   cla_surplus_persons,cla_quota_num,type,date)
           values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s,%s);
        """
        params = (
            self["city"], self["cla_name"], self["cla_teacher_names"],
            self["cla_start_date"], self["cla_end_date"], self["d_name"],
            self["cla_tutor_real_name"],self["cla_classtime_names"], self["cla_classroom_name"],
            self["cla_venue_name"],self["cla_area_name"],self["cla_price"],
            self["dept_name"],self["cla_surplus_persons"],self["cla_quota_num"],self['_type'],self["date"],
        )
        return sql, params













