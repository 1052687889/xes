# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals
import requests
from scrapy.log import logger
import requests
from xes.utils.user_agent import random_user_agent
class XesSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class XesDownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


from scrapy import signals
import redis

class Myextensions(object):

    # 爬虫关闭时 调用本方法
    def spider_closed(self):
        self.rcon.set('hkes_api_status', 1)
        for i in range(3):
            res = requests.get('http://118.31.173.223:8008/api/tal/calculate_xes_apidata/')
            if "success" in res.text:
                break

    def spider_opened(self):
        self.rcon = redis.StrictRedis(host='118.31.173.223', port=6379, db=10)
        self.rcon.set('hkes_api_status', 0)

    @classmethod
    def from_crawler(cls, crawler):
        obj = cls()
        if crawler.spider.name == "xes":
            crawler.signals.connect(obj.open, signal=signals.spider_opened)
            crawler.signals.connect(obj.close, signal=signals.spider_closed)
        return obj

    def open(self, spider):
        print('opened')
        self.spider_opened()

    def close(self, spider):
        print('closed')
        self.spider_closed()


class RandomUserAgentMiddlware(object):
    def process_request(self, request, spider):
        if spider.name == 'xes_app':
            return
        for key,value in spider.headers.items():
            request.headers.setdefault(key, value)
        request.headers.setdefault('User-Agent', random_user_agent())

class RandomProxyMiddleware(object):
    #动态设置ip代理
    def process_request(self, request, spider):
        try:
            get_ip = requests.get('http://47.97.38.188:4000/random_ip/',timeout=10).text
            request.meta["proxy"] = "http://" + get_ip
        except Exception as e:
            logger.error('RandomProxyMiddleware:%s'%(e,))
