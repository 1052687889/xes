#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Author:taoke
import pymysql
from _datetime import datetime,date

# def get_create_table_sql(_date:date):
#     exe_str = '''
#             CREATE TABLE `xes_data_%s` (
#           `id` int(11) NOT NULL AUTO_INCREMENT,
#           `city` varchar(255) DEFAULT NULL,
#           `grade` varchar(255) DEFAULT NULL,
#           `subject` varchar(255) DEFAULT NULL,
#           `level` varchar(255) DEFAULT NULL,
#           `term` varchar(255) DEFAULT NULL,
#           `lesson` varchar(255) DEFAULT NULL,
#           `teacher` varchar(255) DEFAULT NULL,
#           `price` float DEFAULT NULL,
#           `class_name` varchar(255) DEFAULT NULL,
#           `start_date` varchar(255) DEFAULT NULL,
#           `end_date` varchar(255) DEFAULT NULL,
#           `time` varchar(255) DEFAULT NULL,
#           `class_time_long` float DEFAULT NULL,
#           `class_addr` varchar(255) DEFAULT NULL,
#           `state` varchar(255) DEFAULT NULL,
#           `class_num` int(11) DEFAULT NULL,
#           `all_class_time_long` float DEFAULT NULL,
#           `date` date DEFAULT NULL,
#           `hash` varchar(255) DEFAULT NULL,
#           PRIMARY KEY (`id`) USING BTREE,
#           KEY `price` (`price`) USING BTREE,
#           KEY `date` (`date`) USING BTREE,
#           KEY `state` (`state`) USING BTREE,
#           KEY `lesson` (`lesson`) USING BTREE
#         ) ENGINE=InnoDB AUTO_INCREMENT=1582125 DEFAULT CHARSET=utf8;'''%(date,)

# MYSQL_HOST = "218.244.138.88"
# MYSQL_USER = "spiderdb"
# MYSQL_PASSWORD = "Cqmyg321"
# MYSQL_DATABASES = "spiderdb"
# MYSQL_PORT = 13456
# MYSQL_CHARSET = 'utf8'

MYSQL_HOST = "localhost"
MYSQL_USER = "root"
MYSQL_PASSWORD = "123456"
MYSQL_DATABASES = "spiderdb"
MYSQL_PORT = 3306
MYSQL_CHARSET = 'utf8'

def get_create_table_sql(_date:date):
    return '''CREATE TABLE `xes_city_%s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ujis;'''%(_date.strftime('%Y_%m_%d'),)

conn = pymysql.Connect(   host=MYSQL_HOST,
                        user=MYSQL_USER,
                   password=MYSQL_PASSWORD,
                   db=MYSQL_DATABASES,
                   port=MYSQL_PORT,
                   charset=MYSQL_CHARSET)


cur = conn.cursor()
cur.execute('select distinct(date) from xes_data;')
res = cur.fetchall()
# print(res)
for _date in res:
    _date = _date[0]
    # exe_str = 'create table xes_data_%s select * from xes_data where date="%s";'%(_date.strftime('%Y_%m_%d'),_date)
    exe_str = 'select distinct(CONCAT(city,class_addr)) from xes_data where date="%s";'%(_date,)
    cur.execute(exe_str)
    res = cur.fetchall()
    exe_str = get_create_table_sql(_date)
    cur.execute(exe_str)
    for addr in res:
        addr = addr[0]
        exe_str = '''insert into xes_city_%s(addr)
                     values ("%s");'''%(_date.strftime('%Y_%m_%d'),addr)
        cur.execute(exe_str)
    print(len(res),res)


conn.close()

















